// app.js
var express = require('express');
var app = express();
//var subdomain = require('express-subdomain');
const fs = require("fs");
var server = require('http').createServer(app);
var io = require('socket.io')(server);
var dateFormat = require('dateformat');
//var router = express.Router();

io.sockets.setMaxListeners(0);
app.use("/templates", express.static(__dirname + '/templates'));
app.get('/', function(req, res){
	res.sendFile(__dirname + '/index.html');
});

io.on('connection', function(socket){
	socket.emit('clientdate', function(cd){
		console.log(cd);
});

	setInterval(function(){ //actual time (head)
		var now = new Date();
		socket.emit('date', {'date': dateFormat(now, "yyyy-mm-dd, HH:MM:ss")}, 1000);

	});
	//console.log(clients_date);
	var d = new Date(); //left to weekend
	var hour = d.getHours();
	var day = d.getDay();
	console.log(day);
	var upToNowHour = ((day-1)*24) + hour;
	var onePercent = 120/100;
	var progressbar = upToNowHour/onePercent+"%";
	var daysleft = 5 - day;
	var hoursLeft = 120-upToNowHour;

	socket.emit('day', {day:day, progressbar: progressbar, daysleft:daysleft, valuenow:upToNowHour, hoursleft:hoursLeft })

});
io.on('connection', function(socket){
	//CONSOLE:
	console.log('a user connected');
});

//app.use(subdomain('weekendmeter', router))
server.listen(3001, function(){
	console.log('listening on *:3001')
});
